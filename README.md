# RDS 

## Contains

- RDS postgres setup
- Security group for database

## Outputs

## How to use

### Setup Module

```
module "database" {
  source                = "git@gitlab.com:terraform147/database.git"
  db_size               = ""
  db_password           = ""
  encrypted             = false
  name                  = ""
  subnets               = []
  subnets_cidrs         = []
  vpc_id                = ""
}
```

### Import module

```
terraform init
```

## Give a star :stars:

## Want to improve or fix something? Fork me and send a MR! :punch: