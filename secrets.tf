resource "random_password" "db_master_pass" {
  length  = 64
  special = false
}

resource "aws_ssm_parameter" "db_master_pass" {
  name   = "/${var.name}/general/DB_MASTER_PASS"
  type   = "SecureString"
  key_id = module.kms_key.id
  value  = random_password.db_master_pass.result
}