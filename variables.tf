variable "bastion_cidr" {
  description = "Cidr of the subnet where the bastion host is running"
  default     = ""
}

variable "backup_retention_days" {
  description = "Amount of days to save backups"
  default     = 0
}

variable "database_engine" {
  description = "Database engine: mysql, postgres"
  type        = string
}

variable "database_engine_version" {
  description = "Database engine version"
  type        = string
}

variable "database_port" {
  description = "Database port"
  type        = string
}

variable "database_user" {
  description = "Database user"
  type        = string
}

variable "db_size" {
  description = "Size in gb of database"
  type        = string
  default     = "20"
}

variable "encrypted" {
  description = "Enable encryption of database"
  type        = bool
  default     = false
}

variable "instance_type" {
  description = "Instance type for postgres"
  default     = "db.t2.micro"
}

variable "name" {
  description = "Name of application"
  type        = string
}

variable "vpc_id" {
  description = "Vpc id to setup bastion"
  type        = string
}
